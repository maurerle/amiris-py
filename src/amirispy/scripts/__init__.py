# SPDX-FileCopyrightText: 2022 German Aerospace Center <amiris@dlr.de>
#
# SPDX-License-Identifier: CC0-1.0

from .amiris import amiris_cli


def amiris():
    amiris_cli()
