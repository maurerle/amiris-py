<!-- SPDX-FileCopyrightText: 2023 German Aerospace Center <amiris@dlr.de>

SPDX-License-Identifier: CC0-1.0 -->
# Changelog

## [1.4](https://gitlab.com/dlr-ve/esy/amiris/amiris-py/-/tags/v1.4) - TBA
### Changed
- Loosen dependency restrictions for fameio (#39 @dlr-cjs)
- Replace setup.py with pyproject.toml (#13 @dlr-cjs)

## [1.3](https://gitlab.com/dlr-ve/esy/amiris/amiris-py/-/tags/v1.3) - 2023-06-13
### Changed
- Updated dependency to latest fameio version (#36 @dlr-cjs)

### Removed
- Removed JDK8 support and updated AMIRIS artifact downloading during install (#38 @dlr-cjs)

## [1.2](https://gitlab.com/dlr-ve/esy/amiris/amiris-py/-/tags/v1.2) - 2023-04-21
### Changed
- When writing outputs during `amiris run`, TimeStep is now converted from Fame TimeStep to datetime by default (#30 @dlr_fn)
- When writing outputs during `amiris run`, results are merged to hourly values using `fameio merge-time` (#31 (@dlr_fn)

### Added 
- Added `amiris batch` for running multiple scenarios (#32 @dlr_elghazi, @dlr_fn)

## [1.1.4](https://gitlab.com/dlr-ve/esy/amiris/amiris-py/-/tags/v1.1.4) - 2023-02-24
### Added
- Added `-m/--mode` option to `amiris install` for model only `-m/--mode model` or [examples](https://gitlab.com/dlr-ve/esy/amiris/examples) only with `-m/--mode examples` (#27 @dlr_fn)

## [1.1.3](https://gitlab.com/dlr-ve/esy/amiris/amiris-py/-/tags/v1.1.3) - 2023-02-20
### Added
- Added option to `-f/--force` install overwriting existing AMIRIS installation (#26 @dlr_fn)

### Fixed
- `amiris run` not working on linux due to improper check of access rights (#29 @dlr_fn, @dlr-cjs)

## [1.1.2](https://gitlab.com/dlr-ve/esy/amiris/amiris-py/-/tags/v1.1.2) - 2023-02-07
### Added
- Added check for required Java installation (#25 @dlr_fn)
- Added check for sufficient writing access in directories (#18 @dlr_fn)
- Added `" "` to paths to improve identification of paths in logs

## [1.1.1](https://gitlab.com/dlr-ve/esy/amiris/amiris-py/-/tags/v1.1.1) - 2023-01-27
### Fixed
- `amiris run` not working on Mac OS X (#24 @dlr_fn)

## [1.1](https://gitlab.com/dlr-ve/esy/amiris/amiris-py/-/tags/v1.1) - 2022-12-07
### Changed
- **Breaking**: Compatibility with AMIRIS >= v1.2.3.4
- Moved to new AMIRIS packaging with executable Jar and prepackaged log4j.properties

## [1.0](https://gitlab.com/dlr-ve/esy/amiris/amiris-py/-/tags/v1.0) - 2022-11-02
_Initial release_