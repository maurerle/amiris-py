# SPDX-FileCopyrightText: 2023 German Aerospace Center <amiris@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0


def assert_exception_contains(expected: str, e_info) -> None:
    """Asserts that the given ExceptionInfo `e_info` contains given `expected` string - ignoring any placeholders"""
    removed_placeholders = expected.split(sep="{}")
    for message_part in removed_placeholders:
        assert message_part in str(e_info)


def assert_log_contains(expected: str, log: str) -> None:
    """Asserts that given `log` text contains given `expected` string - ignoring any placeholders"""
    assert_exception_contains(expected, log)
