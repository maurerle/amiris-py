# SPDX-FileCopyrightText: 2023 German Aerospace Center <amiris@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
from pathlib import Path
from typing import List

import pytest
from fameio.source.validator import ValidationException

from amirispy.scripts.subcommands.batch import (
    find_valid_scenarios,
    _ERR_ALL_PATHS_INVALID,
    _ERR_NO_VALID_FAME_SCENARIO,
    _ERR_PATH_NOT_EXISTING,
)
from amirispy.source.cli import DEFAULT_PATTERN
from tests.utils import assert_exception_contains, assert_log_contains


class Test:
    VALID_DIR = "tests/data/valid_yaml"
    INVALID_DIR = "tests/data/invalid_yaml"

    @pytest.mark.parametrize(
        "paths, expected",
        [
            ([Path(f"{VALID_DIR}/scenario.yaml")], [Path(f"{VALID_DIR}/scenario.yaml")]),
            ([Path(f"{VALID_DIR}/inner/scenario.yml")], [Path(f"{VALID_DIR}/inner/scenario.yml")]),
            (
                [Path(f"{VALID_DIR}/inner/scenario.yml"), Path(f"{VALID_DIR}/scenario.yaml")],
                [Path(f"{VALID_DIR}/inner/scenario.yml"), Path(f"{VALID_DIR}/scenario.yaml")],
            ),
            ([Path(f"{VALID_DIR}")], [Path(f"{VALID_DIR}/scenario.yaml")]),
            (
                [Path(f"{VALID_DIR}/inner/scenario.yml"), Path(f"{VALID_DIR}")],
                [Path(f"{VALID_DIR}/inner/scenario.yml"), Path(f"{VALID_DIR}/scenario.yaml")],
            ),
        ],
    )
    def test_find_valid_scenarios_non_recursive(self, paths, expected):
        result = find_valid_scenarios(paths, False, DEFAULT_PATTERN)
        self.assert_lists_similar(result, expected)

    @staticmethod
    def assert_lists_similar(result: list, expected: list) -> None:
        """Compares two lists for equality ignoring_order"""
        result.sort()
        expected.sort()
        assert result == expected

    def test_find_valid_scenarios_returns_file_even_if_pattern_not_matching(self):
        file_path = Path(f"{self.VALID_DIR}/scenario.yaml")
        result = find_valid_scenarios([file_path], False, "*.xml")
        assert result == [file_path]

    def test_find_valid_scenarios_raises_on_empty_search(self, caplog):
        file_path = Path(f"not_existing_file.yaml")
        with pytest.raises(ValueError) as e_info:
            find_valid_scenarios([file_path], False, DEFAULT_PATTERN)
        assert_exception_contains(_ERR_ALL_PATHS_INVALID, e_info)
        assert_log_contains(_ERR_PATH_NOT_EXISTING, caplog.text)

    @pytest.mark.parametrize(
        "paths, expected",
        [
            ([Path(f"{VALID_DIR}/scenario.yaml"), Path("does_not_exist.file")], [Path(f"{VALID_DIR}/scenario.yaml")]),
            ([Path("missing/folder/"), Path(f"{VALID_DIR}/scenario.yaml")], [Path(f"{VALID_DIR}/scenario.yaml")]),
        ],
    )
    def test_find_valid_scenarios_ignores_missing_files_and_folders(self, paths, expected, caplog):
        result = find_valid_scenarios(paths, False, DEFAULT_PATTERN)
        self.assert_lists_similar(result, expected)

    def test_find_valid_scenarios_ignores_invalid_files(self, caplog):
        paths = [Path(f"{self.VALID_DIR}/scenario.yaml"), Path(f"{self.INVALID_DIR}/scenario.yaml")]
        expected = [Path(f"{self.VALID_DIR}/scenario.yaml")]
        result = find_valid_scenarios(paths, False, DEFAULT_PATTERN)
        self.assert_lists_similar(result, expected)
        assert_log_contains(_ERR_NO_VALID_FAME_SCENARIO, caplog.text)

    @pytest.mark.parametrize(
        "paths, pattern, expected",
        [
            (
                [Path(f"{VALID_DIR}/inner")],
                DEFAULT_PATTERN,
                [Path(f"{VALID_DIR}/inner/scenario.yml"), Path(f"{VALID_DIR}/inner/my_other_input.yaml")],
            ),
            (
                [Path(f"{VALID_DIR}/inner")],
                "scenario.*",
                [Path(f"{VALID_DIR}/inner/scenario.yml"), Path(f"{VALID_DIR}/inner/scenario.file")],
            ),
            (
                [Path(f"tests/data")],
                "*",
                [
                    Path(f"{VALID_DIR}/inner/scenario.yml"),
                    Path(f"{VALID_DIR}/inner/scenario.file"),
                    Path(f"{VALID_DIR}/inner/my_other_input.yaml"),
                    Path(f"{VALID_DIR}/scenario.yaml"),
                ],
            ),
        ],
    )
    def test_find_valid_scenarios_recursive_pattern_match(self, paths, pattern, expected):
        result = find_valid_scenarios(paths, True, pattern)
        self.assert_lists_similar(result, expected)
